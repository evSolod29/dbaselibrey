﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DBaseLibrary.Extensions
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<TResult> LeftJoin<TLeft, TRight, TKey, TResult>(this IEnumerable<TLeft> left, IEnumerable<TRight> right, Func<TLeft, TKey> leftKey, Func<TRight, TKey> rightKey,
            Func<TLeft, TRight, TResult> result)
        {
            return left.GroupJoin(right, leftKey, rightKey, (l, r) => new { l, r })
                .SelectMany(
                    o => o.r.DefaultIfEmpty(),
                    (l, r) => new { lft = l.l, rght = r })
                .Select(o => result.Invoke(o.lft, o.rght));
        }

        public static IEnumerable<TResult> RightJoin<TLeft, TRight, TKey, TResult>(this IEnumerable<TLeft> left, IEnumerable<TRight> right, Func<TLeft, TKey> leftKey, Func<TRight, TKey> rightKey,
            Func<TLeft, TRight, TResult> result)
        {
            return right.GroupJoin(left, rightKey, leftKey,(r, l) => new { r, l })
                .SelectMany(
                    o => o.l.DefaultIfEmpty(),
                    (r, l) => new { rght = r.r, lft = l })
                .Select(o => result.Invoke(o.lft, o.rght));
        }

        public static IEnumerable<TResult> FullOuterJoin<TLeft, TRight, TKey, TResult>(
            this IEnumerable<TLeft> left, 
            IEnumerable<TRight> right, 
            Func<TLeft, TKey> leftKey, Func<TRight, TKey> rightKey,
            Func<TLeft, TRight, TResult> result)
        {
            return left.LeftJoin(right, leftKey, rightKey, result).Union(left.RightJoin(right, leftKey, rightKey, result));
        }
    }
}
