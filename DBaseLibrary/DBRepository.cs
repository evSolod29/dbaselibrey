﻿using DBaseLibrary.Attributes;
using DBaseLibrary.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DBaseLibrary
{
    /// <summary>
    /// Класс для работы с Dbase или FoxPro базами данных
    /// </summary>
    /// <typeparam name="T">Объект представляющий сущность базы данных</typeparam>
    public abstract class DBRepository<T> : IRepository<T> where T:DBModel
    {
        private string _database;
        private string _table;
        private readonly Func<T> newInstance;

        /// <summary>
        /// Свойство идентификатора
        /// </summary>
        protected internal readonly Property _identifier;

        /// <summary>
        /// Свойства сущности
        /// </summary>
        protected internal readonly Dictionary<Field, Property> _properties;

        /// <summary>
        /// Конструктор, который иницилизирует обязательные поля 
        /// </summary>
        /// <param name="dbFolder">Путь к папке с БД</param>
        /// <param name="dbName">Наименование БД</param>
        public DBRepository(string dbFolder, string dbName)
        {
            _database = dbFolder;
            _table = dbName;
            _properties = new();
            newInstance = Expression.Lambda<Func<T>>(Expression.New(typeof(T))).Compile();
            foreach (var property in typeof(T).GetProperties())
            {
                var field = property.GetCustomAttribute(typeof(Field)) as Field;
                if (field is Field && !field.IsIdentifier)
                {
                    _properties.Add(field, new Property(property));
                }
                else if (field is Field && field.IsIdentifier && _identifier == null)
                {
                    _identifier = new Property(property);
                }
            }
            if (_properties.Count == 0 && _identifier == null) throw new TypeInitializationException($"Класс {typeof(T).Name} не является моделью сущности БД", new Exception());
        }

        /// <summary>
        /// Строка подключения к базе данных
        /// </summary>
        public string Database { get => _database; private set => _database = value; }

        /// <summary>
        /// Используемая таблица
        /// </summary>
        public string Table { get => _table; private set => _table = value; }

        private T GetT(T obj, OleDbDataReader reader, decimal id = 0)
        {
            foreach (var key in _properties.Keys)
            {
                try
                {
                    _properties[key].SetValue(obj, reader.GetValue(reader.GetOrdinal(key.Name)));
                }
                catch (Exception ex)
                {
                    ex.Data.Add("Field Name", key.Name);
                    ex.Data.Add("Row Number", reader.RecordsAffected);
                    throw;
                }
            }
            if (_identifier != null)
                _identifier.SetValue(obj, id);
            return obj;
        }

        private T GetRecord(string query)
        {
            using var oleDbConnection = DBaseControl.GetConnection(Database);
            var obj = newInstance.Invoke();
            oleDbConnection.Open();
            using var oleDbCommand = new OleDbCommand(query, oleDbConnection);
            using var reader = oleDbCommand.ExecuteReader();
            if (reader != null && reader.Read())
            {
                var id = (decimal)reader.GetValue(reader.GetOrdinal("identifier"));
                return GetT(obj, reader, id);
            }
            return default;
        }

        private List<T> GetRecords(string query, List<object> requestValue = null)
        {
            using var oleDbConnection = DBaseControl.GetConnection(Database);
            List<T> result = new();
            oleDbConnection.Open();
            using var oleDbCommand = new OleDbCommand(query, oleDbConnection);
            if (requestValue is not null)
            {
                for (int i = 0; i < requestValue.Count; i++)
                {
                    oleDbCommand.Parameters.AddWithValue(i.ToString(), requestValue[i]);
                }
            }
            using var reader = oleDbCommand.ExecuteReader();
            while (reader != null && reader.Read())
            {
                var obj = newInstance.Invoke();
                var id = (decimal)reader.GetValue(reader.GetOrdinal("identifier"));
                result.Add(GetT(obj, reader, id));
            }
            return result;
        }

        /// <summary>
        /// Добавление объекта в БД.
        /// </summary>
        /// <param name="obj">Объект для добавления</param>
        /// <param name="oleDbConnection">Подключение OleDb</param>
        protected internal void AddRecord(T obj, OleDbConnection oleDbConnection)
        {
            object locker = new();
            string values = string.Join(", ", DBFieldsNameToCollection().Select(x => "?"));
            string query = $"INSERT INTO {Table} ({DBFieldsNameToString()}) VALUES ({values})";
            var logMessage = $"В {Path.Combine(Database, Table)}.dbf добавлена новая запись: {obj}";
            ExecuteNonQuery(query, logMessage, obj: obj,oleDbConnection: oleDbConnection);
            if (_identifier != null)
                _identifier.SetValue(obj, GetLastAddedId());
        }

        protected internal void ExecuteNonQuery(string query, string logMessage, List<object> requestValue = null,
            T obj = null, Dictionary<PropertyInfo, object> properties = null, OleDbConnection oleDbConnection = null)
        {
            bool withConnection = false;
            try
            {
                if (oleDbConnection is null)
                    oleDbConnection = DBaseControl.GetConnection(Database);
                else
                    withConnection = true;
                if(oleDbConnection.State == System.Data.ConnectionState.Closed)
                    oleDbConnection.Open();
                using var oleDbCommand = new OleDbCommand(query, oleDbConnection);
                if (obj is not null)
                {
                    if (properties is not null)
                        foreach (var key in properties.Keys)
                        {
                            var fieldName = _properties.FirstOrDefault(k => k.Value.Name == key.Name).Key.Name;
                            oleDbCommand.Parameters.AddWithValue(fieldName, properties[key]);
                        }
                    else
                        foreach (var key in _properties.Keys)
                            oleDbCommand.Parameters.AddWithValue(key.Name,
                                ReplaceNullValue(_properties[key], _properties[key].GetValue(obj)));
                }
                if (requestValue is not null)
                    for (int i = 0; i < requestValue.Count; i++)
                        oleDbCommand.Parameters.AddWithValue(i.ToString(), requestValue[i]);
                oleDbCommand.ExecuteNonQuery();
                Log.Write(LogType.Update, logMessage);
            }
            catch (Exception ex)
            {
                throw ThrowException(ex);
            }
            finally
            {
                if (!withConnection)
                    oleDbConnection.Dispose();
            }
        }

        /// <summary>
        /// Получение элемента с заданным Id
        /// </summary>
        /// <param name="id">Идентификатор записи (порядковый номер записи)</param>
        /// <returns>Объект с заданным Id</returns>
        public T GetItem(decimal id)
        {
            var query = $"SELECT recno() as identifier, {DBFieldsNameToString()} FROM {Table} WHERE recno() = {id}";
            try
            {
                var obj = GetRecord(query);
                Log.Write(LogType.Info, $"Получена запись {obj} из {Path.Combine(Database, Table)}.dbf");
                return obj;
            }
            catch (Exception ex)
            {
                throw ThrowException(ex);
            }
        }

        /// <summary>
        /// Получение последнего добавленного элемента
        /// </summary>
        /// <returns>Последний добавленный в бд элемент</returns>
        public T GetLastAddedItem()
        {
            var query = $"SELECT recno() as identifier, {DBFieldsNameToString()} FROM {Table} WHERE recno() = RECCOUNT()";
            try
            {
                var obj = GetRecord(query);
                Log.Write(LogType.Info, $"Получена запись {obj} из {Path.Combine(Database, Table)}.dbf");
                return obj;
            }
            catch (Exception ex)
            {
                throw ThrowException(ex);
            }
        }

        /// <summary>
        /// Получение идентификатора последнего добавленного в бд элемента
        /// </summary>
        /// <returns>Идентификатора последнего элемента</returns>
        public decimal GetLastAddedId()
        {
            decimal result = -1;
            var query = "SELECT recno() FROM " + Table + " WHERE recno() = RECCOUNT()";
            try
            {
                using (var oleDbConnection = DBaseControl.GetConnection(Database))
                {
                    oleDbConnection.Open();
                    using OleDbCommand oleDbCommand = new(query, oleDbConnection);
                    using var reader = oleDbCommand.ExecuteReader();
                    if (reader != null && reader.Read())
                    {
                        result = (decimal)reader.GetValue(0);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ThrowException(ex);
            }
        }

        /// <summary>
        /// Метод для получения всех объектов в БД.
        /// </summary>
        /// <returns>Коллекцию элементов БД</returns>
        public List<T> GetItems()
        {
            var query = $"SELECT recno() as identifier, {DBFieldsNameToString()} FROM {Table}";
            try
            {
                var result = GetRecords(query);
                Log.Write(LogType.Info,
                    $"Получено {result.Count} записей из {Path.Combine(Database, Table)}.dbf");
                return result;
            }
            catch (Exception ex)
            {
                throw ThrowException(ex);
            }
        }
        /// <summary>
        /// Изменение объекта в БД.
        /// </summary>
        /// <param name="newObj">Новый объект для замены. Id нового объекта и заменяемого объекта должны совпадать.</param>
        public void UpdateItem(T newObj)
        {
            if (_identifier == null)
                throw new MissingFieldException("Не найдено свойство, в котором Field.IsIdentifier равно true");
            var oldObj = GetItem((decimal)_identifier.GetValue(newObj));
            string query = $"UPDATE {Table} SET {GetRequestString()} WHERE recno() = ?";
            var logMesssage = $"Изменена запись под номером {_identifier.GetValue(newObj)} в {Path.Combine(Database, Table)}: {(oldObj as DBModel).DifferencesBetweenEntries(newObj)}";
            ExecuteNonQuery(query, logMesssage, new List<object> { _identifier.GetValue(newObj) }, newObj);
        }

        /// <summary>
        /// Обновление необходимых полей по заданному условию
        /// </summary>
        /// <param name="obj">Объект с заданными необходимыми полями</param>
        /// <param name="expression">Выражение условий</param>
        /// <param name="isFullUpdate">Полное обновление</param>
        public void UpdateItemsWhere(T obj, Expression<Func<T, bool>> expression, bool isFullUpdate = true)
        {
            DBModel model = obj as DBModel;

            Dictionary<PropertyInfo, object> props = new();
            if (isFullUpdate)
            {
                foreach (var prop in _properties.Values)
                    props.Add(prop.Info, prop.GetValue(obj));
            }
            else
            {
                if (model.PropertyChangedQueue is null && model.PropertyChangedQueue.Count == 0)
                    throw new ArgumentException("Не найдено полей для изменения");
                foreach (var key in model.PropertyChangedQueue.Keys)
                {
                    props.Add(key, model.PropertyChangedQueue[key]);
                    model.PropertyChangedQueue.Remove(key);
                }
            }
            
            string requestString = "";
            List<object> requestValue = new();

            //Обработка условия
            GetRequestString(expression.Body, ref requestString, ref requestValue);
            string query = $"UPDATE {Table} SET {string.Join(" ,", props.Select(x => _properties.FirstOrDefault(k => k.Value.Name == x.Key.Name).Key.Name + " = ?"))} WHERE {requestString}";
            var logMessage = $"Записи изменены, где {GetRequestStringWithValue(requestString, requestValue)}, из {Path.Combine(Database, Table)}.dbf";
            ExecuteNonQuery(query, logMessage, requestValue, obj, props);
        }

        /// <summary>
        /// Обновление необходимых полей по заданному условию
        /// </summary>
        /// <param name="values">Строка с заданными необходимыми полями</param>
        /// <param name="whereString">Строка условий</param>
        public void UpdateItemsWhere(string values, string whereString)
        {
            string query = $"UPDATE {Table} SET ({values}) WHERE {whereString}";
            var logMessage = $"Записи изменены, где {whereString}, из {Path.Combine(Database, Table)}.dbf";
            ExecuteNonQuery(query, logMessage);
        }

        /// <summary>
        /// Добавление объекта в БД.
        /// </summary>
        /// <param name="obj">Объект для добавления</param>
        public void AddItem(T obj)
        {
            using var oleDbConnection = DBaseControl.GetConnection(Database);
            oleDbConnection.Open();
            AddRecord(obj, oleDbConnection);
        }

        /// <summary>
        /// Добавление коллекции объектов в БД. 
        /// </summary>
        /// <param name="objects">Коллекция объектов</param>
        public void AddItems(IEnumerable<T> objects)
        {
            using var oleDbConnection = DBaseControl.GetConnection(Database);
            oleDbConnection.Open();

            foreach (var obj in objects)
                AddRecord(obj, oleDbConnection);
        }

        /// <summary>
        /// Удаления объекта из БД.
        /// </summary>
        /// <param name="id">Идентификатор записи (порядковый номер записи)</param>
        public void DeleteItem(decimal id)
        {
            var item = GetItem(id);
            string query = $"DELETE FROM {Table} WHERE recno() = ?";
            var logMessage = $"Удалена запись {item} из {Path.Combine(Database, Table)}";
            ExecuteNonQuery(query, logMessage, new List<object> { id });
        }

        /// <summary>
        /// Выбор объектов из бд, удовлетворяющих условию
        /// </summary>
        /// <param name="expression">Условие выбора объектов</param>
        /// <returns>Коллекция элементов БД</returns>
        public List<T> GetItemsWhere(Expression<Func<T, bool>> expression)
        {
            string requestString = "";
            List<object> requestValue = new();
            GetRequestString(expression.Body, ref requestString, ref requestValue);
            var query = $"SELECT recno() as identifier, {DBFieldsNameToString()} FROM {Table} WHERE {requestString}";
            try
            {
                var result = GetRecords(query, requestValue);
                Log.Write(LogType.Info, $"Получено {result.Count} зап., где {GetRequestStringWithValue(requestString, requestValue)}, из {Path.Combine(Database, Table)}");
                return result;
            }
            catch (Exception ex)
            {
                throw ThrowException(ex);
            }
        }

        /// <summary>
        /// Выбор объектов из бд, удовлетворяющих условию
        /// </summary>
        /// <param name="expression">Условие выбора объектов</param>
        /// <returns>Коллекция элементов БД</returns>
        public List<T> GetItemsWhere(string expression)
        {
            var query = $"SELECT recno() as identifier, {DBFieldsNameToString()} FROM {Table} WHERE {expression}";
            try
            {
                var result = GetRecords(query);
                Log.Write(LogType.Info, $"Получено {result.Count} зап., где {expression}, из {Path.Combine(Database, Table)}");
                return result;
            }
            catch (Exception ex)
            {
                throw ThrowException(ex);
            }
        }

        /// <summary>
        /// Удаление объектов из бд, удовлетворяющих условию
        /// </summary>
        /// <param name="expression">Условие удаления объектов</param>
        public void DeleteItemsWhere(Expression<Func<T, bool>> expression)
        {
            string requestString = "";
            List<object> requestValue = new();
            GetRequestString(expression.Body, ref requestString, ref requestValue);
            string query = $"DELETE FROM " + Table + " WHERE " + requestString;
            var logMessage = $"Записи удалены, где {GetRequestStringWithValue(requestString, requestValue)}, из {Path.Combine(Database, Table)}.dbf";
            ExecuteNonQuery(query, logMessage, requestValue);
        }

        /// <summary>
        /// Получение кол-ва элементов, удовлетворяющих условию
        /// </summary>
        /// <param name="expression">условие выбора</param>
        /// <returns>Количество элементов, удовлетворяющих условию</returns>
        public int GetItemsCount(Expression<Func<T, bool>> expression)
        {
            string requestString = "";
            List<object> requestValue = new();

            //Обработка условия
            GetRequestString(expression.Body, ref requestString, ref requestValue);

            string query = $"SELECT COUNT(*) FROM {Table} WHERE {requestString}";
            int result;
            try
            {
                using (var oleDbConnection = DBaseControl.GetConnection(Database))
                {
                    oleDbConnection.Open();

                    using var oleDbCommand = new OleDbCommand(query, oleDbConnection);
                    for (int i = 0; i < requestValue.Count; i++)
                    {
                        oleDbCommand.Parameters.AddWithValue(i.ToString(), requestValue[i]);
                    }
                    result = int.Parse(oleDbCommand.ExecuteScalar().ToString());
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ThrowException(ex);
            }
        }

        /// <summary>
        /// Метод формирования условия SQL запроса на основе переданного выражения
        /// </summary>
        /// <param name="func">Выражение(условие)</param>
        /// <param name="result">Сформированная строка условий для SQL запроса</param>
        /// <param name="values">Значения свойств</param>
        // Если нет представления как это работает, то лучше ничего не менять.
        protected void GetRequestString(Expression func, ref string result, ref List<object> values)
        {
            if (func is BinaryExpression)
            {
                var expr = func as BinaryExpression;
                result += "(";
                GetRequestString(expr.Left, ref result, ref values);
                result += $" {expr.NodeType} ";
                GetRequestString(expr.Right, ref result, ref values);
                result += ")";
            }
            else if (func is UnaryExpression)
            {
                if (func.NodeType == ExpressionType.Not)
                {
                    result += $"{func.NodeType} ";
                    GetRequestString((func as UnaryExpression).Operand, ref result, ref values);
                }
                else if (func.NodeType == ExpressionType.Convert || func.NodeType == ExpressionType.ConvertChecked)
                {
                    GetRequestString((func as UnaryExpression).Operand, ref result, ref values);
                }
            }
            else if ((func is MemberExpression) && (func as MemberExpression).Expression is ParameterExpression)
            {
                var propExpr = func as MemberExpression;
                var type = propExpr.Expression as ParameterExpression;
                var prop = type.Type.GetProperty(propExpr.Member.Name);
                if (prop.GetCustomAttribute(typeof(Field)) is not Field dbAttr)
                {
                    throw new Exception($"Выбраное поле {prop.Name} не является полем бд {Table}.dbf");
                }
                string dbField;
                if (dbAttr.IsIdentifier)
                    dbField = "recno()";
                else
                    dbField = dbAttr.Name;
                result += dbField;
            }
            else if ((func is MethodCallExpression && (func as MethodCallExpression).Method.Name == "Contains"))
            {
                var lambdaEx = func as MethodCallExpression;
                values.Add($"*{(lambdaEx.Arguments[0] as ConstantExpression).Value}*");
                result += $"LIKE(?, ";
                GetRequestString(lambdaEx.Object, ref result, ref values);
                result += ")";
            }
            else if ((func is MemberExpression) && (func as MemberExpression).Type.BaseType == typeof(ValueType) && (func as MemberExpression).Member.Name == "Value")
            {
                GetRequestString((func as MemberExpression).Expression, ref result, ref values);
            }
            else if ((func is MemberExpression) && (func as MemberExpression).Member.DeclaringType == typeof(DateTime) &&
                (((func as MemberExpression).Member.Name == "Month") || (func as MemberExpression).Member.Name == "Year"))
            {
                result += (func as MemberExpression).Member.Name + "(";
                GetRequestString((func as MemberExpression).Expression, ref result, ref values);
                result += ")";
            }
            else
            {
                var toObj = Expression.Lambda(func).Compile().DynamicInvoke();
                var obj = toObj;
                try
                {
                    var converter = TypeDescriptor.GetConverter(func.Type);
                    if (func.Type == typeof(DateTime?) && obj == null)
                        obj = new DateTime(1899, 12, 30);
                    if (func.Type == typeof(string) && obj == null)
                        obj = "";
                    if (func.Type == typeof(decimal) && obj == null)
                        obj = 0M;
                    if (obj == null)
                        throw new NullReferenceException();
                    var parse = converter.ConvertFromString(obj.ToString());
                    result += "?";
                    values.Add(obj);
                }
                catch (NullReferenceException)
                {
                    result += "?";
                    if (func.Type.IsGenericType && func.Type == typeof(DateTime?))
                        values.Add(new DateTime(1899, 12, 30));
                    else if (func.Type.IsGenericType && func.Type == typeof(decimal?))
                        values.Add(0);
                    else if (func.Type == typeof(string))
                        values.Add("");
                }
                catch
                {
                    result += "?";
                    values.Add(obj.ToString());
                }
            }
            result = result.Replace(ExpressionType.LessThanOrEqual.ToString(), "<=").Replace(ExpressionType.GreaterThanOrEqual.ToString(), ">=")
                .Replace(ExpressionType.NotEqual.ToString(), "<>").Replace(ExpressionType.Equal.ToString(), "=").Replace(ExpressionType.AndAlso.ToString(), "and")
                .Replace(ExpressionType.GreaterThanOrEqual.ToString(), ">=").Replace(ExpressionType.GreaterThan.ToString(), ">").Replace(ExpressionType.LessThan.ToString(), "<")
                .Replace(ExpressionType.OrElse.ToString(), "or").Replace(ExpressionType.Not.ToString() + " ", "!");
        }

        /// <summary>
        /// Получение наименования полей с " = ?" на конце
        /// </summary>
        /// <returns>Строка наименований</returns>
        public string GetRequestString() => string.Join(", ", _properties.Keys.Select(x => x.Name + " = ?"));

        private static string GetRequestStringWithValue(string requestString, List<object> requestValue)
        {
            StringBuilder @string = new(requestString);
            int k = 0;
            for (int i = 0; i < @string.Length; i++)
            {
                if (@string[i] == '?')
                {
                    @string.Replace("?", requestValue[k].ToString(), i, 1);
                    k++;
                }
            }
            return @string.ToString();
        }

        /// <summary>
        /// Генерация исключения, с записью в лог-файл
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        protected internal Exception ThrowException(Exception ex)
        {
            Log.WriteException(ex);
            if (ex is DbException dbException)
                return DBaseLibrary.DBaseControl.HandleKnownDBaseExceptions(dbException);
            else
                return ex;
        }

        /// <summary>
        /// Получения наименования полей
        /// </summary>
        /// <returns>Коллекция наименования полей</returns>
        protected internal IEnumerable<string> DBFieldsNameToCollection() => _properties.Keys.Select(x => x.Name);

        /// <summary>
        /// Наименование полей через запятую
        /// </summary>
        /// <returns>Строка наименований полей</returns>
        protected internal string DBFieldsNameToString() => string.Join(", ", _properties.Keys.Select(x => x.Name));

        /// <summary>
        /// Замена null на значение по умолчанию
        /// </summary>
        /// <param name="prp">Свойство</param>
        /// <param name="value">Значение</param>
        /// <returns>Если value - null, то значение поумолчанию, иначе - значение</returns>
        protected internal object ReplaceNullValue(Property prp, object value)
        {
            if (value is DateTime date)
            {
                var nullDt = new DateTime(1899, 12, 30);
                if (date < nullDt)
                    date = nullDt;
                return date;
            }
            else if(value is string str)
            {
                return str ?? "";
            }
            else if (prp.Info.PropertyType == typeof(DateTime?))
            {
                var nullDt = new DateTime(1899, 12, 30);
                if ((value as DateTime?).HasValue && (value as DateTime?).Value < nullDt)
                    value = nullDt;
                else if (!(value as DateTime?).HasValue)
                    value = nullDt;
            }
            return value;
        }
        
    }
}
