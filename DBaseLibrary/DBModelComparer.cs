﻿using DBaseLibrary.Utils;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text.Json;

namespace DBaseLibrary
{
    public class DBModelComparer : IEqualityComparer<DBModel>
    {
        private readonly JsonSerializerOptions options = new()
        {
            WriteIndented = true,
            Converters =
            {
                new StringConverter(),
                new DecimalConverter(),
                new DateTimeConverter(),
                new NullableDateTimeConverter()
            }
        };

        public bool Equals(DBModel x, DBModel y) => GetHashCode(x) == GetHashCode(y);

        public int GetHashCode([DisallowNull] DBModel obj)
            => JsonSerializer.Serialize(obj, obj.GetType(), options).GetHashCode();
        
        
    }
}
