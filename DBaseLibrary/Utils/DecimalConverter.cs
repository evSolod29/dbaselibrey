﻿using System;
using System.Globalization;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace DBaseLibrary.Utils
{
    public class DecimalConverter : JsonConverter<decimal>
    {
        public override decimal Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) 
            => decimal.Parse(reader.GetString(),CultureInfo.GetCultureInfo("en-US"));

        public override void Write(Utf8JsonWriter writer, decimal value, JsonSerializerOptions options)
        {
            var str = value.ToString(CultureInfo.GetCultureInfo("en-US"));
            if (str.Contains("."))
            {
                str = str.TrimEnd('0');
                if (str[str.Length - 1] == '.')
                    str = str.Remove(str.Length - 1);
            }
            writer.WriteStringValue(str);
        }
    }
}
