﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace DBaseLibrary.Utils
{
    public class StringConverter: JsonConverter<string>
    {
        public override bool HandleNull => true;

        public override string Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) 
            => reader.GetString();

        public override void Write (Utf8JsonWriter writer, string text, JsonSerializerOptions options) 
            => writer.WriteStringValue(text == null ? "" : text.Trim());
    }
}
