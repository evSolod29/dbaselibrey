﻿using System;
using System.DirectoryServices.AccountManagement;
using System.IO;

namespace DBaseLibrary.Utils
{
    public static class Log
    {
        private static string _logDirectoryPath;
        private readonly static string _userName = UserPrincipal.Current.DisplayName;

        /// <summary>
        /// Путь к каталогу с файлами лога
        /// </summary>
        public static string LogDirectoryPath
        {
            get => string.IsNullOrEmpty(_logDirectoryPath) ? AppDomain.CurrentDomain.BaseDirectory : _logDirectoryPath;
            set
            {
                if (!string.IsNullOrEmpty(value)) 
                {
                    if (!Directory.Exists(value))
                        try
                        {
                            Directory.CreateDirectory(value);
                            _logDirectoryPath = value;
                        }
                        catch
                        {
                            throw new DirectoryNotFoundException("Указан неверный путь к каталогу");
                        }
                    else
                        _logDirectoryPath = value;
                }
            }
        }
        /// <summary>
        /// Наименование файла.
        /// </summary>
        public static string FileName => DateTime.Now.ToString("dd-MM-yyyy") + ".log";

        /// <summary>
        /// Полный путь к файлу лога.
        /// </summary>
        public static string FilePath => Path.Combine(LogDirectoryPath, FileName);

        /// <summary>
        /// Имя пользователя.
        /// </summary>
        public static string UserName  => _userName;

        /// <summary>
        /// Добавления сообщения в файл лога.
        /// </summary>
        /// <param name="type">тип сообщения</param>
        /// <param name="message">текст сообщения</param>
        public static void Write(LogType type, string message)
        { 
            message = $"[{UserName}] [{DateTime.Now:HH:mm}] {type}: {message}" + Environment.NewLine;
            File.AppendAllText(FilePath, message);
        }

        /// <summary>
        /// Добавление сообщения об исключении в файл лога
        /// </summary>
        /// <param name="ex">Сообщение об ошибке</param>
        public static void WriteException(Exception ex)
        {
            var dataString = "";
            if (ex.Data is not null && ex.Data.Count != 0)
            {
                dataString = "\nОтладочные данные :";
                foreach (var key in ex.Data.Keys)
                {
                    dataString += $"\n{key} - {ex.Data[key]}";
                }
            }
            var message = $"[{UserName}] [{DateTime.Now:HH:mm}] {LogType.Error}: {ex.Message}\n{ex.StackTrace}\n{Environment.StackTrace}\n{dataString}" + Environment.NewLine;
            File.AppendAllText(FilePath, message);
        }
    }

    public enum LogType 
    { 
        Debug,
        Info,
        Error,
        Insert,
        Update,
        Delete
    }
}
