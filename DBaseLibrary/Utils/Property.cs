﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DBaseLibrary.Utils
{
    public class Property
    {
        private readonly PropertyGetter getter;
        private readonly PropertySetter setter;
        public string Name { get; private set; }

        public PropertyInfo Info { get; private set; }

        public Property(PropertyInfo propertyInfo)
        {
            if (propertyInfo == null)
                throw new NullReferenceException("Property cannot be empty");
            this.Name = propertyInfo.Name;
            this.Info = propertyInfo;
            if (this.Info.CanRead)
            {
                this.getter = new PropertyGetter(propertyInfo);
            }

            if (this.Info.CanWrite)
            {
                this.setter = new PropertySetter(propertyInfo);
            }
        }


        /// <summary>
        　　 ///Get the value of the object
        /// </summary>
        　　/// <param name="instance"></param>
        　　/// <returns></returns>
        public object GetValue(object instance)
        {
            return getter?.Invoke(instance);
        }


        /// <summary>
        　　 ///Assignment operation
        /// </summary>
        　　/// <param name="instance"></param>
        　　/// <param name="value"></param>
        public void SetValue(object instance, object value)
        {
            this.setter?.Invoke(instance, value);
        }

        //private static readonly ConcurrentDictionary<Type, Property[]> securityCache = new ConcurrentDictionary<Type, Property[]>();

        //public static Property[] GetProperties(Type type)
        //{
        //    return securityCache.GetOrAdd(type, t => t.GetProperties().Select(p => new Property(p)).ToArray());
        //}

    }

    /// <summary>
    　　///Property get operation class
    /// </summary>
    public class PropertyGetter
    {
        private readonly Func<object, object> funcGet;

        public PropertyGetter(PropertyInfo propertyInfo) : this(propertyInfo?.DeclaringType, propertyInfo.Name)
        {

        }

        public PropertyGetter(Type declareType, string propertyName)
        {
            if (declareType == null)
            {
                throw new ArgumentNullException(nameof(declareType));
            }
            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }



            this.funcGet = CreateGetValueDeleagte(declareType, propertyName);
        }


        //Code core
        private static Func<object, object> CreateGetValueDeleagte(Type declareType, string propertyName)
        {
            // (object instance) => (object)((declaringType)instance).propertyName

            var param_instance = Expression.Parameter(typeof(object));
            var body_objToType = Expression.Convert(param_instance, declareType);
            var body_getTypeProperty = Expression.Property(body_objToType, propertyName);
            var body_return = Expression.Convert(body_getTypeProperty, typeof(object));
            return Expression.Lambda<Func<object, object>>(body_return, param_instance).Compile();
        }

        public object Invoke(object instance)
        {
            return this.funcGet?.Invoke(instance);
        }
    }


    public class PropertySetter
    {
        private readonly Action<object, object> setFunc;

        public PropertySetter(PropertyInfo property)
        {
            if (property == null)

            {
                throw new ArgumentNullException(nameof(property));
            }
            this.setFunc = CreateSetValueDelagate(property);
        }



        private static Action<object, object> CreateSetValueDelagate(PropertyInfo property)
        {
            // (object instance, object value) => 
            //  ((instanceType)instance).Set_XXX((propertyType)value)

            //Parameters required to declare methods
            var param_instance = Expression.Parameter(typeof(object));
            var param_value = Expression.Parameter(typeof(object));

            var body_instance = Expression.Convert(param_instance, property.DeclaringType);
            var body_value = Expression.Convert(param_value, property.PropertyType);
            var body_call = Expression.Call(body_instance, property.GetSetMethod(), body_value);

            return Expression.Lambda<Action<object, object>>(body_call, param_instance, param_value).Compile();
        }

        public void Invoke(object instance, object value)
        {
            this.setFunc?.Invoke(instance, value);
        }
    }
}
