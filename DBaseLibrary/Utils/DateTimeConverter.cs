﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace DBaseLibrary.Utils
{
    public class DateTimeConverter: JsonConverter<DateTime>
    {
        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) 
            => DateTime.Parse(reader.GetString());

        public override void Write (Utf8JsonWriter writer, DateTime date, JsonSerializerOptions options) 
            => writer.WriteStringValue(date < new DateTime(1899,12,30) ? new DateTime(1899, 12, 30) : date);
    }

    public class NullableDateTimeConverter : JsonConverter<DateTime?>
    {
        public override DateTime? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var result = reader.GetString();
            if (result.ToLower().Trim() == "null")
                return new DateTime(1899, 12, 30);
            return DateTime.Parse(result);
        }        

        public override void Write(Utf8JsonWriter writer, DateTime? date, JsonSerializerOptions options)
        {
            if (date.HasValue && date.Value <= new DateTime(1899, 12, 30))
                writer.WriteNullValue();
            else
                writer.WriteStringValue(date.Value);
        }         
    }
}
