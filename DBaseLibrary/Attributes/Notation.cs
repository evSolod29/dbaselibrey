﻿using System;
using System.Runtime.CompilerServices;

namespace DBaseLibrary.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class Notation : Attribute
    {
        private string _notation;

        public Notation([CallerMemberName] string notation = null) => _notation = notation;

        public string NotationProp { get => _notation; private set => _notation = value; }
    }
}
