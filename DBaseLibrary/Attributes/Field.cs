﻿using System;
using System.Runtime.CompilerServices;

namespace DBaseLibrary.Attributes
{
    [AttributeUsage(AttributeTargets.Property,AllowMultiple = false)]
    public class Field : Attribute
    {
        private string _name;
        private bool _isIdentifier;
        private string _propertyName;

        public Field([CallerMemberName] string fieldName = "", bool isIdentifier = false, [CallerMemberName] string propertyName = "")
        {
            _name = fieldName;
            _isIdentifier = isIdentifier;
            _propertyName = propertyName;
        }

        /// <summary>
        /// Наименование поля в базе данных
        /// </summary>
        public string Name { get => _name; private set => _name = value; }

        /// <summary>
        /// Является значением recno()
        /// </summary>
        public bool IsIdentifier { get => _isIdentifier; private set => _isIdentifier = value; }
        public string PropertyName { get => _propertyName; set => _propertyName = value; }
    }
}
