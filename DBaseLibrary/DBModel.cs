﻿using DBaseLibrary.Attributes;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DBaseLibrary
{
    public class DBModel : INotifyPropertyChanged, INotifyDataErrorInfo, IFormattable, ICloneable
    {
        private decimal _id;
        [Field(isIdentifier: true)]
        public decimal Id { get => _id; set => SetProperty(ref _id, value); }

        public override string ToString()
        {
            int countFields = GetType().GetProperties().Length;
            var fields = GetType().GetProperties();
            StringBuilder str_build = new();
            for (int i = 0; i < countFields; i++)
            {
                if (fields[i].GetCustomAttribute(typeof(JsonIgnoreAttribute)) == null)
                {
                    string str = fields[i].Name + ": " + (fields[i].GetValue(this) == null ? "" : fields[i].GetValue(this).ToString());
                    str_build.AppendFormat("  {0,5}", str);
                }
            }
            return str_build.ToString();
        }

        public string DifferencesBetweenEntries(object secondObj)
        {
            StringBuilder @string = new();
            var fields = GetType().GetProperties().Where(x => x.GetCustomAttributes(false).Where(i => (i as Field) != null).Any());
            if (fields.Count() <= 1)
            {
                throw new FormatException($"Невозможно сравнить {this.GetType().Name}, т.к. объект не является моделью cущности БД");
            }
            foreach (var field in fields)
            {
                if (field.Name != "Id")
                {
                    var firstValue = field.GetValue(this);
                    var secondValue = field.GetValue(secondObj);
                    if ((firstValue == null && secondValue != null) || (firstValue != null && secondValue == null))
                    {
                        var first = field.GetValue(this) == null ? "null" : field.GetValue(this).ToString();
                        var second = field.GetValue(secondObj) == null ? "null" : field.GetValue(secondObj).ToString();
                        if (second == "30.12.1899 0:00:00") continue;
                        @string.Append($"{field.Name} - c {first} на {second},");
                        continue;
                    }

                    if (string.IsNullOrEmpty(firstValue.ToString()) && string.IsNullOrEmpty(secondValue.ToString())) continue;
                    if (firstValue.ToString().Trim() != secondValue.ToString().Trim()) @string.Append($"{field.Name} - c {field.GetValue(this)} на {field.GetValue(secondObj)},");
                }
            }
            if (@string.Length != 0) @string.Remove(@string.Length - 1, 1);
            return @string.ToString();
        }

        #region Реализация INotifyPropertyChanged

        private readonly object _lock = new();
        public event PropertyChangedEventHandler PropertyChanged;

        [JsonIgnore]
        public Dictionary<PropertyInfo, object> PropertyChangedQueue{ get; private set; }

        /// <summary>
        /// Метод, указывающий, что свойство изменилось
        /// </summary>
        /// <param name="prop">Имя свойства</param>
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            var property = GetType().GetProperty(prop);
            if (property != null)
            {
                if (PropertyChangedQueue == null)
                    PropertyChangedQueue = new();

                if (PropertyChangedQueue.ContainsKey(property))
                    PropertyChangedQueue[property] = property.GetValue(this);
                else
                    PropertyChangedQueue.Add(property, property.GetValue(this));
            }
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        /// <summary>
        /// Установка значения свойства, с проверкой изменилось ли значени
        /// </summary>
        /// <typeparam name="T">Тип свойства</typeparam>
        /// <param name="member">Переменная для записи</param>
        /// <param name="value">Новое значение</param>
        /// <param name="propertyName">Наименование свойства</param>
        /// <returns>Возвращает true, если значение изменилось. Иначе - false </returns>
        protected virtual bool SetProperty<T>(ref T member, T value, [CallerMemberName] string propertyName = null)
        {
            //var comparer = new DBModelComparer();
            //if (comparer.Equals(member as DBModel, value as DBModel))
            //{
            //    return false;
            //}

            member = value;
            OnPropertyChanged(propertyName);    
            return true;
        }

        #endregion
        #region Реализация INotifyDataErrorInfo
        /// <summary>
        /// Словарь ошибок, где ключ - имя свойства, а значение - коллекция сообщений об ошибке
        /// </summary>
        private readonly ConcurrentDictionary<string, List<string>> _errors = new();

        /// <summary>
        /// Событие вызываемое при изменении коллекции _errors
        /// </summary>
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        public void OnErrorsChanged(string propertyName)
            => ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        
        /// <summary>
        /// Получение списка ошибок
        /// </summary>
        /// <param name="propertyName">Имя свойства</param>
        /// <returns></returns>
        public IEnumerable GetErrors(string propertyName)
        {
            _errors.TryGetValue(propertyName, out List<string> errorsForName);
            return errorsForName;
        }

        /// <summary>
        /// Свойство для проверки наличия ошибок
        /// </summary>
        [JsonIgnore]
        public bool HasErrors => _errors.Any(kv => kv.Value != null && kv.Value.Count > 0);    

        public Task ValidateAllAsync() => Task.Run(() => ValidateAll());

        /// <summary>
        /// Проверка свойства на ошибки
        /// </summary>
        /// <param name="prop">Наименование свойства</param>
        public void Validate([CallerMemberName] string prop = "")
        {
            var property = GetType().GetProperty(prop);
            var messages = new List<string>();
            foreach (ValidationAttribute attr in property.GetCustomAttributes(typeof(ValidationAttribute), true))
            {
                var val = property.GetValue(this);
                var validationResult = attr.GetValidationResult(val, new ValidationContext(this));
                var isValid = validationResult == ValidationResult.Success;

                if (_errors.ContainsKey(prop))
                {
                    _errors.TryRemove(prop, out _);
                }
                if (!isValid)
                {
                    messages.Add(validationResult.ErrorMessage);
                }
            }
            if (messages.Count > 0) _errors.TryAdd(prop, messages);
            OnErrorsChanged(prop);
        }

        /// <summary>
        /// Проверка всех свойств модели на ошибки
        /// </summary>
        public void ValidateAll()
        {
            lock (_lock)
            {
                var validationContext = new ValidationContext(this);
                var validationResults = new List<ValidationResult>();
                Validator.TryValidateObject(this, validationContext, validationResults, true);

                foreach (var kv in _errors.ToList())
                {
                    if (validationResults.All(r => r.MemberNames.All(m => m != kv.Key)))
                    {
                        _errors.TryRemove(kv.Key, out List<string> outLi);
                        OnErrorsChanged(kv.Key);
                    }
                }

                var aaa = validationResults.GroupBy(x => x.MemberNames).ToList();

                foreach (var prop in aaa)
                {
                    var messages = prop.Select(r => r.ErrorMessage).ToList();
                    var a = prop.Key.FirstOrDefault();
                    if (_errors.ContainsKey(a))
                    {
                        _errors.TryRemove(prop.Key.First(), out List<string> outLi);
                    }
                    _errors.TryAdd(prop.Key.First(), messages);
                    OnErrorsChanged(prop.Key.First());
                }
            }
        }
        #endregion
        #region Реализация IFormattable
        public string ToString(string format) => this.ToString(format, CultureInfo.CurrentCulture);

        public string ToString(string format, IFormatProvider formatProvider)
        {
            switch (format)
            {
                //using Notation class
                case "N":
                    {
                        StringBuilder str_build = new();
                        foreach (var property in GetType().GetProperties())
                        {
                            if (property.GetCustomAttributes(typeof(Notation), true).Length != 0)
                            {
                                foreach (Notation attr in property.GetCustomAttributes(typeof(Notation), true))
                                {
                                    string str = attr.NotationProp + ": " + (property.GetValue(this) == null ? "" : property.GetValue(this).ToString());
                                    str_build.AppendFormat("  {0,5}", str);
                                }
                            }
                            else
                            {
                                string str = property.Name + ": " + (property.GetValue(this) == null ? "" : property.GetValue(this).ToString());
                                str_build.AppendFormat("  {0,5}", str);
                            }
                        }
                        return str_build.ToString();
                    }
                default: return this.ToString();
            }
        }
        #endregion
        #region Реализация IClonable
        public object Clone() => JsonSerializer.Deserialize(JsonSerializer.Serialize(this, GetType()), GetType());       
        #endregion
    }
}
