﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.OleDb;

namespace DBaseLibrary
{
    /// <summary>
    /// Класс управление соединением с БД (MS SQL сервер и каталог базы данных FoxPro)
    /// </summary>
    public static class DBaseControl
    {
        private const string _connectionPattern = "Provider={0};Data Source={1};";
        private const string _provider = "VFPOLEDB.1";

        /// <summary>
        /// Получение соединения с каталогом базы данных FoxPro
        /// </summary>
        public static OleDbConnection GetConnection(string path) 
            => new(FoxProConnectRow(path));    

        /// <summary>
        /// Получение строки соединения с каталогом базы данных foxpro
        /// </summary>
        private static string FoxProConnectRow(string path) 
            => string.Format(_connectionPattern, _provider, path);   

        /// <summary>
        /// Обработка типовых исключений работы с БД MSSQL и FoxPro, с формированием сообщения о возможной причине.
        /// В случае, если исходное исключение не SqlException, не StorageException или 
        /// не OleDbException - пробрасывается исходное.
        /// </summary>
        public static Exception HandleKnownDBaseExceptions(DbException ex)
        {
            Dictionary<int, string> errors = new()
            {
                {
                    2627,
                    "Одно или несколько полей добавляемой/редактируемой сущности " +
                "помечены признаком уникальности и уже есть в базе данных. Добавление ещё одного экземляра " +
                "сущности запрещено ключом уникальности."
                },
                {
                    547,
                    "Удаление указанной записи запрещено, так как на " +
                "неё есть как минимум одна действующая ссылка из других таблиц базы данных."
                },
                {
                    -2147467259,
                    "Ошибка в SQL-запросе к базе данных FoxPro: несовпадение типов, " +
                "неверное имя столбца, отсутсвие столбца или что-то иное."
                },
                {
                    -2147217865,
                    "Нет доступа к файлу (или отсутвует файл) базы " +
                "данных FoxPro для выполнения запроса."
                }
            };

            var isDbException = ex is OleDbException;

            if (!isDbException)
            {
                return ex; // Исключение является не SqlException, не StorageException, не OleDbException
            }

            var probableCause = string.Empty; // предполагаемая ошибка
            var currentInnerException = ex;

            do
            {
                if (errors.ContainsKey(currentInnerException.ErrorCode))
                {
                    probableCause = errors[currentInnerException.ErrorCode];
                    break;
                }
                currentInnerException = currentInnerException.InnerException as DbException;
            } while (currentInnerException != null);

            // Если неизвестная причина SqlException или OleDbException
            if (string.Empty.Equals(probableCause))
            {
                return ex;
            }

            // Если SqlException или OleDbException с "возможной причиной"
            return new Exception(ex.Message + $"Возможная причина: {probableCause}", ex);
        }
    }
}
