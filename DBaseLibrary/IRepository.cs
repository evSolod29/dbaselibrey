﻿using DBaseLibrary.Utils;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DBaseLibrary
{
    /// <summary>
    /// Интерфейс для работы с базами данных
    /// </summary>
    /// <typeparam name="T">Объект представляющий сущность базы данных</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Строка подключения к базе данных
        /// </summary>
        public string Database { get; }

        /// <summary>
        /// Используемая таблица
        /// </summary>
        public string Table { get; }

        /// <summary>
        /// Получение элемента с заданным Id
        /// </summary>
        /// <param name="id">Идентификатор записи (порядковый номер записи)</param>
        /// <returns>Объект с заданным Id</returns>
        public T GetItem(decimal id);

        /// <summary>
        /// Получение последнего добавленного элемента
        /// </summary>
        /// <returns>Последний добавленный в бд элемент</returns>
        public T GetLastAddedItem();

        /// <summary>
        /// Получение идентификатора последнего добавленного в бд элемента
        /// </summary>
        /// <returns>Идентификатора последнего элемента</returns>
        public decimal GetLastAddedId();

        /// <summary>
        /// Метод для получения всех объектов в БД.
        /// </summary>
        /// <returns>Коллекцию элементов БД</returns>
        public List<T> GetItems();

        /// <summary>
        /// Изменение объекта в БД.
        /// </summary>
        /// <param name="newObj">Новый объект для замены. Id нового объекта и заменяемого объекта должны совпадать.</param>
        public void UpdateItem(T newObj);

        /// <summary>
        /// Обновление необходимых полей по заданному условию
        /// </summary>
        /// <param name="obj">Объект с заданными необходимыми полями</param>
        /// <param name="expression">Выражение условий</param>
        /// <param name="isFullUpdate">Полное обновление</param>
        public void UpdateItemsWhere(T obj, Expression<Func<T, bool>> expression, bool isFullUpdate);

        /// <summary>
        /// Обновление необходимых полей по заданному условию
        /// </summary>
        /// <param name="values">Строка с заданными необходимыми полями</param>
        /// <param name="whereString">Строка условий</param>
        public void UpdateItemsWhere(string values, string whereString);

        /// <summary>
        /// Добавление объекта в БД.
        /// </summary>
        /// <param name="obj">Объект для добавления</param>
        public void AddItem(T obj);

        /// <summary>
        /// Добавление коллекции объектов в БД. 
        /// </summary>
        /// <param name="objects">Коллекция объектов</param>
        public void AddItems(IEnumerable<T> objects);

        /// <summary>
        /// Удаления объекта из БД.
        /// </summary>
        /// <param name="id">Идентификатор записи (порядковый номер записи)</param>
        public void DeleteItem(decimal id);

        /// <summary>
        /// Выбор объектов из бд, удовлетворяющих условию
        /// </summary>
        /// <param name="expression">Условие выбора объектов</param>
        /// <returns>Коллекция элементов БД</returns>
        public List<T> GetItemsWhere(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Выбор объектов из бд, удовлетворяющих условию
        /// </summary>
        /// <param name="expression">Условие выбора объектов</param>
        /// <returns>Коллекция элементов БД</returns>
        public List<T> GetItemsWhere(string expression);

        /// <summary>
        /// Удаление объектов из бд, удовлетворяющих условию
        /// </summary>
        /// <param name="expression">Условие удаления объектов</param>
        public void DeleteItemsWhere(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Получение кол-ва элементов, удовлетворяющих условию
        /// </summary>
        /// <param name="expression">условие выбора</param>
        /// <returns>Количество элементов, удовлетворяющих условию</returns>
        public int GetItemsCount(Expression<Func<T, bool>> expression);
    }
}
