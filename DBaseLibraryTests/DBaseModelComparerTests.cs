﻿using DBaseLibrary;
using DBaseLibraryTests.Data;
using System;
using System.Collections.Generic;
using Xunit;

namespace DBaseLibraryTests
{
    public class DBaseModelComparerTests
    {
        [Theory]
        [MemberData(nameof(TestObjectsData))]
        public void TestEquals(TestObject x, TestObject y, bool expected)
        {
            var comparer = new DBModelComparer();
            var result = comparer.Equals(x, y);
            Assert.Equal(expected, result);
        }

        public static IEnumerable<object[]> TestObjectsData()
        {
            yield return new object[] 
            { 
                new TestObject() 
                { 
                    Id = 1,
                    Age = 2,
                    Birthday = new DateTime(2015,11,11),
                    Name="Alex",
                    Salary=202.2M
                },
                new TestObject()
                {
                    Id = 1,
                    Age = 2,
                    Birthday = new DateTime(2015,11,11),
                    Name="Alex",
                    Salary=202.2M
                }, 
                true
            };
            yield return new object[]
            {
                new TestObject()
                {
                    Id = 2,
                    Age = 2,
                    Birthday = new DateTime(2015,11,11),
                    Name="Alex",
                    Salary=202.2M
                },
                new TestObject()
                {
                    Id = 1,
                    Age = 2,
                    Birthday = new DateTime(2015,11,11),
                    Name="Alex",
                    Salary=202.2M
                },
                true
            };
            yield return new object[]
            {
                new TestObject()
                {
                    Id = 1,
                    Age = 3,
                    Birthday = new DateTime(2015,11,11),
                    Name="Alex",
                    Salary=202.2M
                },
                new TestObject()
                {
                    Id = 1,
                    Age = 2,
                    Birthday = new DateTime(2015,11,11),
                    Name="Alex",
                    Salary=202.2M
                },
                false
            };
            yield return new object[]
            {
                new TestObject()
                {
                    Id = 1,
                    Age = 2,
                    Birthday = new DateTime(2016,11,11),
                    Name="Alex",
                    Salary=202.2M
                },
                new TestObject()
                {
                    Id = 1,
                    Age = 2,
                    Birthday = new DateTime(2015,11,11),
                    Name="Alex",
                    Salary=202.2M
                },
                false
            };
            yield return new object[]
            {
                new TestObject()
                {
                    Id = 1,
                    Age = 2,
                    Birthday = new DateTime(2016,11,11),
                    Name="Alex",
                    Salary=202.20M
                },
                new TestObject()
                {
                    Id = 1,
                    Age = 2,
                    Birthday = new DateTime(2015,11,11),
                    Name="Alex",
                    Salary=202.2M
                },
                false
            };
        }
    }
}
