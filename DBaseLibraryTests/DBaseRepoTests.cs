using DBaseLibrary;
using DBaseLibraryTests.Data;
using DBaseLibraryTests.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Xunit;

namespace DBaseLibraryTests
{
    [TestCaseOrderer("DBaseLibraryTests.Test.PriorityOrderer", "DBaseLibraryTests")]
    public class DBaseRepoTests
    {
        [Fact, TestPriority(0)]
        public void GetItem()
        {
            var repo = new TestObjectRepo();
            var expected = new TestObject()
            {
                Age = 2,
                Birthday = new DateTime(2015, 11, 11),
                Name = "Alex",
                Salary = 202.2M
            };
            var obj = repo.GetItem(1);
            var comparer = new DBModelComparer();
            Assert.True(comparer.Equals(expected, obj));
        }

        [Fact, TestPriority(0)]
        public void GetLastAddedItemIdentifier()
        {
            var repo = new TestObjectRepo();
            decimal expected = 1;
            var actual = repo.GetLastAddedId();
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(0)]
        public void GetItemsCount()
        {
            var repo = new TestObjectRepo();
            int expected = 1;
            var actual = repo.GetItemsCount(x => true);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(1)]
        public void InsertItem()
        {
            var repo = new TestObjectRepo();
            var obj = new TestObject()
            {
                Age = 2,
                Birthday = new DateTime(2015, 11, 11),
                Name = "Alex",
                Salary = 202.2M
            };
            var expected = (TestObject)obj.Clone();
            repo.AddItem(obj);
            obj = repo.GetLastAddedItem();
            var comparer = new DBaseLibrary.DBModelComparer();
            Assert.True(comparer.Equals(expected, obj));
        }

        [Fact, TestPriority(2)]
        public void DeleteItem()
        {
            var repo = new TestObjectRepo();
            int expected = 1;
            repo.DeleteItem(2);
            var actual = repo.GetItemsCount(x => true);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(3)]
        public void InsertItemWithNullProp()
        {
            var repo = new TestObjectRepo();
            var obj = new TestObject();

            var expected = (TestObject)obj.Clone();
            repo.AddItem(obj);
            obj = repo.GetLastAddedItem();
            var comparer = new DBaseLibrary.DBModelComparer();
            Assert.True(comparer.Equals(expected, obj));
            repo.DeleteItem(repo.GetLastAddedId());
        }

        [Fact, TestPriority(4)]
        public void DeleteAllItem()
        {
            var repo = new TestObjectRepo();
            repo.DeleteItemsWhere(x => true);
            var expected = 0;
            var actual = repo.GetItemsCount(x => true);
            Assert.Equal(expected, actual);
        }

        [Fact, TestPriority(5)]
        public void InsertItems()
        {
            var repo = new TestObjectRepo();
            var list = new List<TestObject>()
            {
                new TestObject()
                {
                    Age = 2,
                    Birthday = new DateTime(2015, 11, 11),
                    Name = "Alex",
                    Salary = 202.2M
                },
                new TestObject()
                {
                    Age = 3,
                    Birthday = new DateTime(2014, 11, 11),
                    Name = "Jack",
                    Salary = 434.27M
                },
                new TestObject()
                {
                    Age = 4,
                    Birthday = new DateTime(2013, 11, 11),
                    Name = "Sam",
                    Salary = 543.22M
                },
                new TestObject()
                {
                    Age = 4,
                    Birthday = new DateTime(2013, 11, 11),
                    Name = "Sam",
                    Salary = 543.22M
                }
            };
            repo.AddItems(list);
            var expected = list.Count;
            var actual = repo.GetItemsCount(x => true);
            Assert.Equal(expected, actual);
            repo.DeleteItemsWhere(x => true);
        }

        [Theory, TestPriority(6), MemberData(nameof(WhereTestObjects))]
        public void GetItemsWhere(List<TestObject> objects, Expression<Func<TestObject, bool>> expression)
        {
            var repo = new TestObjectRepo();
            repo.DeleteItemsWhere(x => true);
            repo.AddItems(objects);
            var actual = repo.GetItemsWhere(expression);
            repo.DeleteItemsWhere(x => true);
            var expected = objects.Where(expression.Compile()).ToList();
            Assert.Equal(expected, actual, new DBaseLibrary.DBModelComparer());
        }
        #region GetItemsWhereData
        public static IEnumerable<object[]> WhereTestObjects()
        {
            yield return new object[]
            {
                new List<TestObject>()
                {
                    new TestObject()
                    {
                        Age = 2,
                        Birthday = new DateTime(2015, 11, 11),
                        Name = "Alex",
                        Salary = 202.2M
                    },
                    new TestObject()
                    {
                        Age = 3,
                        Birthday = new DateTime(2014, 11, 11),
                        Name = "Jack",
                        Salary = 434.27M
                    },
                    new TestObject()
                    {
                        Age = 4,
                        Birthday = new DateTime(2013, 11, 11),
                        Name = "Sam",
                        Salary = 543.22M
                    },
                    new TestObject()
                    {
                        Age = 4,
                        Birthday = new DateTime(2013, 11, 11),
                        Name = "Sam",
                        Salary = 543.22M
                    }
                },
                GetLambdaExpr(x => x.Birthday == new DateTime(2013, 11, 11))
            };
            yield return new object[]
            {
                new List<TestObject>()
                {
                    new TestObject()
                    {
                        Age = 2,
                        Birthday = new DateTime(2015, 11, 11),
                        Name = "Alex",
                        Salary = 202.2M
                    },
                    new TestObject()
                    {
                        Age = 3,
                        Birthday = new DateTime(2014, 11, 11),
                        Name = "Jack",
                        Salary = 434.27M
                    },
                    new TestObject()
                    {
                        Age = 4,
                        Birthday = new DateTime(2013, 11, 11),
                        Name = "Sam",
                        Salary = 543.22M
                    },
                    new TestObject()
                    {
                        Age = 4,
                        Birthday = new DateTime(2013, 11, 11),
                        Name = "Sam",
                        Salary = 543.22M
                    }
                },
                GetLambdaExpr(x => x.Name.Contains("S"))
            };
            yield return new object[]
            {
                new List<TestObject>()
                {
                    new TestObject()
                    {
                        Age = 2,
                        Birthday = new DateTime(2015, 11, 11),
                        Name = "Alex",
                        Salary = 202.2M
                    },
                    new TestObject()
                    {
                        Age = 3,
                        Birthday = new DateTime(2014, 11, 11),
                        Salary = 434.27M
                    },
                    new TestObject()
                    {
                        Age = 4,
                        Birthday = new DateTime(2013, 11, 11),
                        Name = "Sam",
                        Salary = 543.22M
                    },
                    new TestObject()
                    {
                        Age = 4,
                        Name = "Sam",
                        Salary = 543.22M
                    }
                },
                GetLambdaExpr(x => x.Birthday == null || x.Name == null)
            };
        }
        #endregion

        [Fact, TestPriority(7)]
        public void UpdateItemsWhere()
        {
            var repo = new TestObjectRepo();
            repo.DeleteItemsWhere(x => true);
            var list = new List<TestObject>()
                {
                    new TestObject()
                    {
                        Age = 2,
                        Birthday = new DateTime(2015, 11, 11),
                        Name = "Alex",
                        Salary = 202.2M
                    },
                    new TestObject()
                    {
                        Age = 3,
                        Name = "Jack",
                        Birthday = new DateTime(2014, 11, 11),
                        Salary = 434.27M
                    },
                    new TestObject()
                    {
                        Age = 4,
                        Birthday = new DateTime(2013, 11, 11),
                        Name = "Sam",
                        Salary = 543.23M
                    },
                    new TestObject()
                    {
                        Age = 4,
                        Name = "Sam",
                        Salary = 543.22M
                    }
                };
            repo.AddItems(list);
            list.Where(x => x.Name.Contains("Sam")).ToList().ForEach(x =>
            {
                x.Birthday = new DateTime(2014, 11, 11);
            });
            var expected = list;
            repo.UpdateItemsWhere(new TestObject() { Birthday = new DateTime(2014, 11, 11) }, x => x.Name.Contains("Sam"), false);
            var actual = repo.GetItems();
            repo.DeleteItemsWhere(x => true);
            Assert.Equal(expected, actual, new DBaseLibrary.DBModelComparer());
        }

        [Fact, TestPriority(7)]
        public void FullUpdateItemsWhere()
        {
            var repo = new TestObjectRepo();
            repo.DeleteItemsWhere(x => true);
            var list = new List<TestObject>()
                {
                    new TestObject()
                    {
                        Age = 2,
                        Birthday = new DateTime(2015, 11, 11),
                        Name = "Alex",
                        Salary = 202.2M
                    },
                    new TestObject()
                    {
                        Age = 3,
                        Name = "Jack",
                        Birthday = new DateTime(2014, 11, 11),
                        Salary = 434.27M
                    },
                    new TestObject()
                    {
                        Age = 4,
                        Birthday = new DateTime(2013, 11, 11),
                        Name = "Sam",
                        Salary = 543.22M
                    },
                    new TestObject()
                    {
                        Age = 4,
                        Name = "Sam",
                        Salary = 543.22M
                    }
                };
            repo.AddItems(list);
            var obj = list.FirstOrDefault(x => x.Name.Contains("Sam"));
            list[3] = obj;
            obj.Birthday = new DateTime(2014, 11, 11);
            var expected = list;
            repo.UpdateItemsWhere(obj, x => x.Name.Contains("Sam"));
            var actual = repo.GetItems();
            repo.DeleteItemsWhere(x => true);
            Assert.Equal(expected, actual, new DBaseLibrary.DBModelComparer());
        }

        private static Expression<Func<TestObject, bool>> GetLambdaExpr(Expression<Func<TestObject, bool>> expression) => expression;
    }
}
