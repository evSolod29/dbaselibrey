﻿using DBaseLibrary;

namespace DBaseLibraryTests.Data
{
    public class TestObjectRepo :  DBRepository<TestObject>
    {
        public TestObjectRepo() : base("data\\", "testobject")
        {
        }
    }
}
