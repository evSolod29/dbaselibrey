﻿using DBaseLibrary;
using DBaseLibrary.Attributes;
using System;
using System.Text.Json.Serialization;

namespace DBaseLibraryTests.Data
{
    public class TestObject: DBModel
    {
        private decimal _id;
        private string _name;
        private decimal _age;
        private DateTime? _birthday;
        private decimal _salary;

        [JsonIgnore]
        [Field("id",true)]
        public decimal Id { get => _id; set => SetProperty(ref _id, value); }

        [Field("name")]
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        [Field("age")]
        public decimal Age
        {
            get { return _age; }
            set { SetProperty(ref _age, value); }
        }

        [Field("birthday")]
        public DateTime? Birthday
        {
            get { return _birthday; }
            set { SetProperty(ref _birthday, value); }
        }

        [Field("salary")]
        public decimal Salary
        {
            get { return _salary; }
            set { SetProperty(ref _salary, value); }
        }

        public decimal Asas { get => 2; }
    }
}
